//
//  ViewController.swift
//  Calculator
//
//  Created by Pritesh Parekh on 6/17/17.
//  Copyright © 2017 Pritesh Parekh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func one(_ sender: UIButton) {
            label.text = (label.text ?? "") + "1"
    }
    @IBAction func two(_ sender: UIButton) {
         label.text = (label.text ?? "") + "2"
    }
    @IBAction func three(_ sender: Any) {
         label.text = (label.text ?? "") + "3"
    }
    @IBAction func four(_ sender: Any) {
        label.text = (label.text ?? "") + "4"
    }
    @IBAction func five(_ sender: Any) {
        label.text = (label.text ?? "") + "5"
    }
    @IBAction func six(_ sender: Any) {
        label.text = (label.text ?? "") + "6"
    }
    @IBAction func seven(_ sender: Any) {
        label.text = (label.text ?? "") + "7"
    }
    @IBAction func eight(_ sender: Any) {
        label.text = (label.text ?? "") + "8"
    }
    @IBAction func nine(_ sender: Any) {
        label.text = (label.text ?? "") + "9"
    }
    @IBAction func zero(_ sender: Any) {
        label.text = (label.text ?? "") + "0"
    }
    @IBAction func point(_ sender: Any) {
        label.text = (label.text ?? "") + "."
    }
    @IBAction func equals(_ sender: Any) {
        label.text = (label.text ?? "") + " "
    }
    @IBAction func add(_ sender: Any) {
        label.text = (label.text ?? "") + "+"
    }
    @IBAction func minus(_ sender: Any) {
        label.text = (label.text ?? "") + "-"
    }
    @IBAction func multiply(_ sender: Any) {
        label.text = (label.text ?? "") + "*"
    }
    @IBAction func Divide(_ sender: Any) {
        label.text = (label.text ?? "") + "/"
    }
    @IBAction func clear(_ sender: Any) {
        label.text = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

